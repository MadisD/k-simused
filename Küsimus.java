import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Scanner;



public class K�simus {
	
	
	
	
	public static void main(String[] args) throws IOException {
		
		final BufferedWriter out = new BufferedWriter(new OutputStreamWriter(
			    new FileOutputStream("k�simus.html"), "UTF-8"));
		out.write("<html>" + "\n"
				+ "<head>"+ "\n"
				+ "<meta charset=\"UTF-8\">"+ "\n"
						+ "<title>K�simused</title>"+ "\n"
						+ "</head>"+ "\n"
						+ "<body>"+ "\n"
						+ "<h1>K�simused</h1>"+ "\n"
						+ "<form action=\"mailto:someone@example.com\" method=\"post\" enctype=\"text/plain\">"+ "\n"+ "\n"+ "\n"+ "\n");
		

		
		
		esitaK�simused("k�simused.txt",out);
		
		
		
		out.write("<br/>"+ "\n"
				+ "<input type=\"submit\" value=\"Saada vastused\" />"+ "\n"
						+ "</form>"+ "\n"
						+ "</body>"+ "\n"
						+ "</html>"+ "\n");
		
		out.close();
	}
	
	
	public static void esitaK�simused(String fileName, BufferedWriter out) throws IOException{
		
		Scanner sisendTekst = new Scanner(new File(fileName));
		int k�simuseNr = 0;
	
		while (sisendTekst.hasNextLine()){
			String rida = sisendTekst.nextLine().trim();
			if (rida.trim().isEmpty() == false) {
				k�simuseNr++;
				createPieces(rida,out,k�simuseNr);	
			}
		}
		
		sisendTekst.close();
	}
	
	
	//Loon t�kid reast
	public static void createPieces(String rida, BufferedWriter out,int k�simuseNr) throws IOException{
		String k�simus;
		String vastus = null;
		String �igeVastusListist = null;
		int vastuseIndex = 0;
		String[] variandid = null;
		
		
		String[] reaJupid;
		reaJupid = rida.split(";");
		k�simus = reaJupid[0].trim();
		
		switch (reaJupid.length) {
		//Kui on sisend-vastus
		case 2:
			vastus = reaJupid[1].trim();
			
			out.write("<h2>"+k�simus+"</h2>"+ "\n"+ "\n"
					+ "<input name=\"kysimus"+k�simuseNr+"\"/>"+ "\n"+ "\n");
			
			break;
			
		case 3:
			//Kui on Jah/Ei vastus
			if (reaJupid[1].trim().length() == 0) {
				vastus = reaJupid[2].trim();
				
				out.write("<h2>"+k�simus+"</h2>"+ "\n"+ "\n"
						+ "<input type=\"radio\" name=\"kysimus"+k�simuseNr+"\" value=\"jah\">jah"+ "\n"+ "\n"
								+ "<input type=\"radio\" name=\"kysimus"+k�simuseNr+"\" value=\"ei\">ei"+ "\n"+ "\n"+ "\n"+ "\n");
				
			}
			//Kui on valikuvariandid
			else {
				vastuseIndex = Integer.parseInt(reaJupid[2].trim());
				variandid = reaJupid[1].trim().split("/");
				�igeVastusListist = variandid[vastuseIndex-1];
				
				
				out.write("<h2>"+k�simus+"</h2>"+ "\n"+ "\n"
						+ "<select name=\"kysimus"+k�simuseNr+"\">"+ "\n"+ "\n"
								+ "<option></option>"+ "\n"+ "\n"
								+ "<option>"+variandid[0]+"</option>"+ "\n"+ "\n"
								+ "<option>"+variandid[1]+"</option>"+ "\n"+ "\n"
								+ "<option>"+variandid[2]+"</option>"+ "\n"+ "\n"
								+ "</select>"+ "\n"+ "\n");
				
			}
			break;
		}	
		
		printQuestion(k�simus, variandid);
		String userAnswer = getAnswer();
		feedback(userAnswer, vastus, �igeVastusListist);
		
		
	}
	
	
	//Prindin k�simuse kasutajale
	public static void printQuestion(String k�simus,String[] variandid){
		
		System.out.println(k�simus);

		if (variandid != null) {
			
			for (String variant : variandid) {
				System.out.println("\t"+variant);
			}	
		} 
		System.out.println();
	}
	
	
	//Kasutaja vastuse tagastamine
	public static String getAnswer(){
		@SuppressWarnings("resource")
		Scanner sisend = new Scanner(System.in);
		String vastus = sisend.nextLine();
		return vastus;
	}
	
	
	//Kontrolli vastust
	public static boolean checkAnswer(String userAnswer, String rightAnswer, String answerFromList){
		if (rightAnswer != null) {
			if (userAnswer.equalsIgnoreCase(rightAnswer)) {
				return true;
			} else {
				return false;
			}
		} else {
			if (userAnswer.equalsIgnoreCase(answerFromList)) {
				return true;
			} else {
				return false;
			}
		}
	}
	
	
	//Anna tagasisidet
	public static void feedback(String userAnswer, String rightAnswer, String answerFromList){
		if (checkAnswer(userAnswer, rightAnswer, answerFromList)) {
			System.out.println("Sinu vastus oli �ige");
		} else {
			System.out.println("Sinu vastus oli vale");
		}
		System.out.println();
	}
}
